package com.epam.rd.java.basic.task7.db;

abstract class SQLQueryConstant {

    static final String FIND_ALL_USERS = "SELECT * FROM users";
    static final String FIND_ALL_TEAMS = "SELECT * FROM teams";
    static final String FIND_ALL_TEAMS_OF_USER = "SELECT * FROM teams WHERE id IN (SELECT team_id FROM users_teams WHERE user_id = ?)";
    static final String FIND_USER = "SELECT * FROM users WHERE login = ?";
    static final String FIND_TEAM = "SELECT * FROM teams WHERE name = ?";

    static final String INSERT_USER = "INSERT INTO users VALUES (DEFAULT, ?)";
    static final String INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";
    static final String SET_TEAMS_FOR_USER = "INSERT INTO users_teams VALUES (?, ?)";

    static final String DELETE_TEAM = "DELETE FROM teams WHERE name = ?";
    static final String DELETE_USERS = "DELETE FROM users WHERE login = ?";

    static final String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";


    private SQLQueryConstant() {
    }
}
