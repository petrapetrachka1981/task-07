package com.epam.rd.java.basic.task7.db;

public class DBException extends Exception {

	static final String LIST_OF_USERS_NOT_CREATED = "The list of users has not been created";
	static final String LIST_OF_TEAMS_NOT_CREATED = "The list of teams has not been created";
	static final String USER_NOT_INSERTED = "User has not been added";
	static final String TEAM_NOT_INSERTED = "Team has not been added";
	static final String USER_NOT_ADDED_TO_TEAMS = "User has not been added to teams";
	static final String TEAM_NOT_DELETED = "Team has not been deleted";
	static final String USERS_NOT_DELETED = "Users has not been deleted";
	static final String USER_NOT_FOUND = "User is not exist";
	static final String TEAM_NOT_FOUND = "Team is not exist";
	static final String TEAM_NOT_UPDATED = "Team has not been updated";

	public DBException(String message, Throwable cause) {
		super(message, cause);
	}

}
